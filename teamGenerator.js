
import { getPlayers } from "./dreamteamAPI.js";
import { namesList }  from './championsPool.js';
// import { namesList }  from './playersPool.js';
// import { subsOut }  from './subsOut.js';
import { subsOut }  from './championsOut.js';

const BUDGET = 100000000;
const PLAYERS_IN_SAME_TEAM = 2;
const MIN_PRICE = 5000000;
let res;
let remainingMinInPos;
let team;
let teams;
let remainingBudget;
let pool;
let remainPositions
let outs;
let mustPositions = []

const randomElement = (array) => {
    return array[Math.floor(Math.random() * array.length)]
}

const getForbiddenTeams = () => {
    return Object.keys(teams).filter(key => teams[key] == PLAYERS_IN_SAME_TEAM);
}

const randomPlayer = (players, priceFilter, positions) => {
    const forbiddenTeams = getForbiddenTeams();
    players = players
        .filter(player => priceFilter(player.marketValue))
        .filter(player => !forbiddenTeams.includes(player.teamNum.toString()))
        .filter(player => positions.includes(player.type));
    return randomElement(players)
}

const getAviliablePositions = () => {
    if (mustPositions.length > 0) {
        return mustPositions.splice(0, 1);
    }
    return Object.keys(remainPositions).filter(key => remainPositions[key] >= remainingMinInPos);;
}

const putPlayer = (player) => {
    team.push(player)
    remainingBudget -= player.marketValue;
    remainPositions[player.type] -= 1;
    if (Object.keys(teams).includes(player.teamNum.toString())) {
        teams[player.teamNum]++;
    } else {
        teams[player.teamNum] = 1;
    }
    pool = pool.filter(p => p.id != player.id);
    if (remainPositions[player.type] == 0) {
        remainingMinInPos = 1;
    }
    return player;
}

const init = async () => {
    if (!res) {
        res = await getPlayers();
    }
    // pool = res.filter(player => namesList.includes(player.name));
    // pool = namesList.map(name => {
    //     let a = res.find(player => player.name == name)
    //     console.log(`name: ${a.name}`)
    //     return a;
    // });

    try {
        pool = namesList.map(name => {
            let a = res.find(player => player.name == name)
            return a;
        });
    } catch (error) {
        pool = namesList.map(name => {
            console.log(`name: ${player.name}`)
            return res.find(player => player.name == name)
        });
    }
    remainingMinInPos = 0;
    team = []
    teams = {}
    remainingBudget = BUDGET;
    remainPositions = {
        g: 1,
        d: 5,
        m: 5,
        a: 3
    }
}

const fillMustPositions = () => {
    if (remainPositions.g == 1) mustPositions.push('g')
    if (remainPositions.d >= 3) mustPositions.push('d')
    if (remainPositions.m >= 3) mustPositions.push('m')
    if (remainPositions.d >= 4) mustPositions.push('d')
    if (remainPositions.m >= 4) mustPositions.push('m')
    if (remainPositions.d == 5) mustPositions.push('d')
    if (remainPositions.m == 5) mustPositions.push('m')
    if (remainPositions.a == 3) mustPositions.push('a')
}

const put10Player = () => {
    const avg = remainingBudget / (11 - team.length);
    let player = randomPlayer(pool, (n) => n <= avg, getAviliablePositions());
    if (player) {
        return putPlayer(player)
    } else {
        return false;
    }
}

const putLastPlayer = () => {
    let pos = getAviliablePositions();
    let player = randomPlayer(pool, (n) => n == remainingBudget, pos);
    while (!player && remainingBudget >= MIN_PRICE) {
        remainingBudget -= 1000000;
        player = randomPlayer(pool, (n) => n == remainingBudget, pos);
    }
    if (player) {
        return putPlayer(player)
    } else {
        return false; // create team failed
    }
}

const printPlayers = (team) => {
    team.forEach(p => {
        console.log(`name: ${p.name},   price: ${p.marketValue / 1000000}`)
    });
}


const generateTeam = async () => {
    await init();
    let player = randomPlayer(pool, (n) => n > 0, ['g']);
    putPlayer(player)
    player = randomPlayer(pool, (n) => n > 0, ['a']);
    putPlayer(player)

    for (let i = 3; i <= 9; i++) {
        const avg = remainingBudget / (11 - team.length);
        let priceFilter;
        if (i % 2 == 0) {
            priceFilter = (n) => n < avg && n < remainingBudget; 
        } else {
            priceFilter = (n) => n > avg && n < remainingBudget && remainingBudget - n > 13000000; 
        }
        let player = randomPlayer(pool, priceFilter, getAviliablePositions());
        if (player) {
            putPlayer(player);
        } else {
            return null; // create team failed
        }
    }

    if (!put10Player() || !putLastPlayer()) {
        return null;
    }
    
    return team;
}

const generateSubs = async (oldTeam) => {
    await init();
    outs = res.filter(player => subsOut.includes(player.name))
    pool = pool.filter(p => !outs.map(p => p.id).includes(p.id))
                .filter(p => !oldTeam.map(p => p.id).includes(p.id))
    outs = outs.filter(player => oldTeam.map(p => p.name).includes(player.name));
    outs = outs.sort((p1, p2) => p2.marketValue - p1.marketValue).slice(0, 3);
    oldTeam = oldTeam.filter(p => !outs.map(p => p.id).includes(p.id))
    oldTeam.forEach(p => {
        putPlayer(p);
    });
    fillMustPositions()

    if (team.length <= 8) {
        let priceFilter = (n) => remainingBudget - n > 10000000;
        let player = randomPlayer(pool, priceFilter, getAviliablePositions());
        if (player) {
            putPlayer(player);
        } else {
            return null; 
        }
    }
    if (team.length <= 9) {
        const p = put10Player();
        if (!p) {
            return null;
        }
    }
    if (team.length <= 10) {
        const p = putLastPlayer();
        if (!p) {
            return null;
        }
    }

    return team;

    // if there are no 3 players in outs
    // remove players with the lowest number of points (and highest price)
    // put remaining players by the algorithem
    // random X3 captain
    // if 5 subs - use allsubs
}

export { generateTeam, generateSubs }