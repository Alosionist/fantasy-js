import { generateTeam, generateSubs } from "./teamGenerator.js";
import { register, login, createTeam, getMyTeam, makeSubs, getPlayers, joinLeague, isInLeague } from "./dreamteamAPI.js";

const JS_LEAGUE = "146542"
const JS_CHAMPIONS = "303841"

const generate = async () => {
    let result = generateTeam();
    while (!(await result)) {
        result = generateTeam();
        console.log("generate again");
    }
    return result.then(team => {
        const max = team.sort((a, b) => b.marketValue - a.marketValue)[0]
        return team.map(p => {
            return { "id": p.id, "isCaptain": p.id == max.id }
        })
    })  
}

const makeGenerateSubs = async (players) => {
    let result = generateSubs(players);
    let i = 0;
    while (!(await result) && i < 20) {
        result = generateSubs(players);
        i++;
    }
    if (!(await result)) return false;
    return result.then(team => {
        const max = team.sort((a, b) => b.marketValue - a.marketValue)[0]
        return team.map(p => {
            return { "id": p.id, "isCaptain": p.id == max.id }
        })
    })  
}

const makeRegister = async (userId) => {
    let status = (await register(userId)).status;
    if (status == 200) {
        console.log(`registered: (id ${userId})`);
    } else if (status == 400) {
        console.log("register failed with status ", status);
        console.log("trying to login... ");
    } else {
        console.log("Error in register: " + userId);
        throw new Error();
    }
}

const makeLogin = async (userId) => {
    const auth = await login(userId);
    if (!auth) {
        console.log("login failed.");
        throw new Error();
    }
    // console.log(`logged in: (id ${userId})`);
    return auth;
}

const makeCreateTeam = async (auth, userId) => {
    // console.log("creating...");
    const team = await generate();
    // console.log(`team generated.`);
    await createTeam(auth, team);
    console.log(`team created! (id ${userId})`);
}

const doSubs = async (auth, userId, myTeam) => {
    let newTeam = await makeGenerateSubs(myTeam.players);
    if (newTeam) {
        let res = await makeSubs(auth, newTeam);
        // TODO: handle errors better
        if (res.includes('החילופים נקלטו ונשלחו למערכת לאישור.')) {
            console.log(`made subs for userId ${userId}`);
        } else {
            console.log(`could not do subs for userId ${userId}`, res);
        }
    } else  {
        console.log(`cannot generate ${userId}`);
    }
}

function sleep(ms) {
    return new Promise((resolve) => {
      setTimeout(resolve, ms);
    });
} 

let notFound = 0;
let teamsFound = 0;
let leagues = 0;
let LeaguenotFound = 0;
const teams = [];

const getTopTeams = (count) => {
    const sorted = teams.sort((tm1, tm2) => tm2.teamPoints - tm1.teamPoints)
    return sorted.slice(0, count)
}

const printTeam = (team) => {
    team.forEach(p => {
        console.log(`name: ${p.name},   price: ${p.marketValue / 1000000}`)
    });
    console.log("-----------------");
}

const doLoop = async (userId) => {
    let auth;
    try {
        // await makeRegister(userId);
        auth = await makeLogin(userId);

        let myTeam = await getMyTeam(auth);
        if (myTeam) {
            // teams.push({
            //     teamPoints: myTeam.teamPoints,
            //     allowedSubs: myTeam.allowedSubs,
            //     userId
            // });
            
            ++teamsFound

            doSubs(auth, userId, myTeam);
            
            // console.log(`userId: ${userId} was created in: ${new Date (new Date(myTeam.updatedOn / 10000).setYear(2021))} (${teamsFound} teams found)`);
        } else {
            ++notFound
            // console.log(`----- team not created: ${userId} (${notFound} teamsFound not found) -----`);
            // makeCreateTeam(auth, userId);
        }
        // await sleep(2000);

        // let leagueExist = await isInLeague(auth, JS_LEAGUE)
        // if (leagueExist) {
        //     leagues++;
        // } else {
        // await joinLeague(JS_LEAGUE, auth);
        //     LeaguenotFound++;
        // }
        // console.log(res);
        // console.log("-----------------");
    } catch (err) {       
        console.log(`error in user ${userId}.`);
        console.log(err);
    }
}

async function main() { 
    // -- async --
    const promises = [];
    let last = 0;
    const START = 1301;
    const WAIT_FACTOR = 100;
    for (let userId = START; userId <= 2500; userId++) {
        promises.push(doLoop(userId));
        if (userId % WAIT_FACTOR == 0) {
            console.log("waiting...");
            for (let j = 0; j < WAIT_FACTOR; j++) {
                await promises[j + last];
            }
            last = userId - START;
        }
    }
    console.log("end loop");
    for (let j = last; j < promises.length; j++) {
        await promises[j];
    }
    console.log("finished");
    console.log(teamsFound, " found");
    console.log(notFound, " not found");
    // let top10 = getTopTeams(10);
    // console.log(top10);

    // console.log(leagues, " leagues found");
    // console.log(LeaguenotFound, " leagues not found");
/*
    // -- users generator -- 
    for (let userId = 105; userId <= 800; userId++) {
        // let status = (await register(userId)).status;
        // if (status != 200) {
        //     console.log("register failed with status ", status);
        //     console.log("trying to login... ");
        // } else {
        //     console.log(`registered: (id ${userId})`);
        // }
        const auth = await login(userId);
        if (!auth) {
            console.log("login failed.");
            continue;
        }
        console.log(`logged in: (id ${userId})`);

        // console.log(res);
        let myTeam = await getMyTeam(auth);
        if (myTeam) {
            console.log(`userId: ${userId} was created in: ${new Date (new Date(myTeam.updatedOn / 10000).setYear(2021))} (${++teamsFound} teams found)`);
        } else {
            console.log(`----- team not created: ${userId} (${++notFound} teamsFound not found) -----`);
            const team = await generate();
            console.log(`team generated.`);
            let res = await createTeam(auth, team, userId);
            console.log(`team created! (id ${userId})`);
        }
        let leagueExist = await isInLeague(auth, JS_LEAGUE)
        if (leagueExist) {
            console.log("already in league");
        } else {
            await joinLeague(JS_LEAGUE, auth);
            console.log("joined js league!");
        }
        // await sleep(2000);
        console.log("-----------------");
    }*/
}

/*
TODO list:
 - change game by 1 paramether: fantasy or dreamteam
 - improve subs
 - make loop for watch:
   * top points
   * join league
   * check getMyTeam
   * reCreate team (getMyTeam -> createTeam)


*/

main();


