import fetch from "node-fetch";
import fetchTimeout from 'fetch-timeout';
import { teamsNames }  from './teamsNames.js';
import { coachesNames }  from './coachesNames.js';

const MAIL = "alosionist"; // alonsagyas alosionist
const FANTASY_TYPE = "fantasyleague"; // dreamteam fantasyleague

const getPlayers = async () => {
    const url = `https://${FANTASY_TYPE}.sport5.co.il/API/Player/`
    const res = await fetch(url, {
        headers: {
            'Accept': 'application/json'
        }
    }).then(data => data.json()).then(data => JSON.parse(data))
    return res;
}

const register = (userId) => {
    const url = `https://${FANTASY_TYPE}.sport5.co.il/API/Register`
    const data = {
        agreedToRegulations: true,
        captchaValiid: true,
        email: `${MAIL}+${userId}@gmail.com`,
        emailBanner: true,
        name: "alonsa",
        password: "passwordjs",
        repeatPassword: "passwordjs",
        system: "W"
    }
    return fetch(url, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
    })
}

const login = async (userId) => {
    const url = `https://${FANTASY_TYPE}.sport5.co.il/API/Login/Post`
    const data = {"email": `${MAIL}+${userId}@gmail.com`,"password":"passwordjs"}
    const res = await fetchTimeout(url, {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
    }, 10000, 'login timeout error')
    .then(data => data.json())
    return res.jwt_token
}

const getMyTeam = async (auth) => {
    const url = `https://${FANTASY_TYPE}.sport5.co.il/API/Team/GetMyTeam`
    const res = await fetchTimeout(url, {
        headers: {
            'Authorization': 'Bearer ' + auth
        },
    }, 10000, 'getMyTeam timeout error')
    .then(data => data.json())
    .catch(err => {
        return null;
    });
    return res;
}

const randomElement = (array) => {
    return array[Math.floor(Math.random() * array.length)]
}

const createTeam = async (auth, players) => {
    const url = `https://${FANTASY_TYPE}.sport5.co.il/API/Team/CreateTeam`
    const data = {
        "teamName": randomElement(teamsNames),
        "coachName": randomElement(coachesNames),
        "favTeam":100039, //142744 100039
        "players": players,
        "fullSub":false,
        "tripleCaptain": Math.floor(Math.random() * 7) == 0
    }
    
    const res = await fetch(url, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + auth
        },
        body: JSON.stringify(data),
    }).then(data => data.text());
    return res;
}

const makeSubs = async (auth, players) => {
    const url = `https://${FANTASY_TYPE}.sport5.co.il/API/Team/SubsTeam`
    const data = {
        "players":players,
        "fullSub":false,
        "tripleCaptain":false
    }
    return await fetch(url, {
        method: 'POST',
        headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + auth
        },
        body: JSON.stringify(data),
    }).then(data => data.text());
}

const joinLeague = async (leagueId, auth) => {
    const res = await fetch(`https://${FANTASY_TYPE}.sport5.co.il/API/League/JoinLeague/${leagueId}`, {
        method: 'POST',
        headers: {
        'Authorization': 'Bearer ' + auth
        },
    }).then(data => data.text());
    return res;
}

const isInLeague = async (auth, leagueId) => {
    const url = `https://${FANTASY_TYPE}.sport5.co.il/API/League/Get`
    const res = await fetch(url, {
        headers: {
            'Authorization': 'Bearer ' + auth
        }
    }).then(data => data.json());
    return !!JSON.parse(res).leagues.find(lg => lg.id == leagueId)
}

export { getPlayers, register, login, createTeam, getMyTeam, joinLeague, isInLeague, makeSubs }