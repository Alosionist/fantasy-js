// ------------- register ----------
url = "https://dreamteam.sport5.co.il/API/Register"
data = {
    agreedToRegulations: true,
    captchaValiid: true,
    email: "alonsagyas+103@gmail.com",
    emailBanner: true,
    name: "aaaaa",
    password: "password103",
    repeatPassword: "password103",
    system: "W"
}
res = await fetch(url, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(data)
})

// ------------ Login ------------
url = "https://dreamteam.sport5.co.il/API/Login/Post"
data = {"email":"alonsagyas+103@gmail.com","password":"password103"}
res = await fetch(url, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(data)
}).then(data => data.json());
auth = res.jwt_token

// --------- Get Players -----------
const getPlayers = async () => {
    url = "https://dreamteam.sport5.co.il/API/Player/"
    res = await fetch(url, {
        headers: {
            'Accept': 'application/json'
        }
    }).then(data => data.json()).then(data => JSON.parse(data))
    return res;
}

// res = res.substr(res.indexOf(">") + 1)
// res = res.substr(0, res.indexOf("</string>"))
// data = JSON.parse(res)

// ----------- Create team ---------
url = "https://dreamteam.sport5.co.il/API/Team/CreateTeam"
data = {
    "teamName":"aaa.js",
    "coachName":"js",
    "favTeam":100059,
    "players":[
        {"id":10329,"isCaptain":false},
        {"id":22438,"isCaptain":false},
        {"id":5727,"isCaptain":false},
        {"id":36886,"isCaptain":true},
        {"id":7329,"isCaptain":false},
        {"id":1697,"isCaptain":false},
        {"id":23839,"isCaptain":false},
        {"id":37926,"isCaptain":false},
        {"id":11055,"isCaptain":false},
        {"id":9083,"isCaptain":false},
        {"id":10302,"isCaptain":false}
    ],
    "fullSub":false,
    "tripleCaptain":false
}

res = await fetch(url, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + auth
    },
    body: JSON.stringify(data),
}).then(data => data.json());

// ---------- GetMyTeam --------
url = "https://dreamteam.sport5.co.il/API/Team/GetMyTeam"

res = await fetch(url, {
    headers: {
      'Authorization': 'Bearer ' + auth
    },
}).then(data => data.text());

// ----------- Subs -----------
url = "https://dreamteam.sport5.co.il/API/Team/SubsTeam"
data = {
    "players":[
        {"id":37926,"isCaptain":false},
        {"id":11055,"isCaptain":false},
        {"id":22438,"isCaptain":false},
        {"id":5727,"isCaptain":false},
        {"id":23839,"isCaptain":false},
        {"id":10302,"isCaptain":false},
        {"id":7329,"isCaptain":false},
        {"id":9083,"isCaptain":false},
        {"id":10329,"isCaptain":false},
        {"id":36886,"isCaptain":true},
        {"id":1696}
    ],
    "fullSub":false,
    "tripleCaptain":false
}

res = await fetch(url, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + auth
    },
    body: JSON.stringify(data),
}).then(data => data.json());

// -------- getLeague --------

leagueId = 146542;
url = `https://dreamteam.sport5.co.il/API/Rank/League/2?GUID=${leagueId}`

res = await fetch(url, {
    headers: {
      'Accept': 'application/json'
    },
}).then(data => data.json());

// TODO:
// - Generate teams from pool of players
// - create big number of teams
// + implement GetMyTeam
// + implement subs
// - implement Generate auto subs (and then do it for all teams)
// - bonus - join every team to the same league, and implement GetLeague and GetMajorLeague